from django.urls import path
from buku.views import ajax_url, fungsi_data


urlpatterns = [
    path('cari_buku/', ajax_url, name='ajax_url'),
    path('data/', fungsi_data, name='fungsi_data'),
   
]
