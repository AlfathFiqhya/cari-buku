from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import JsonResponse
import requests
import json

#Belajar ajax live search API

def ajax_url(request):
	context = {}
	return render(request, 'ajax/ajax.html',context)

def fungsi_data(request):
	url = 'https://www.googleapis.com/books/v1/volumes?q=' + request.GET['q']
	ret = requests.get(url)
	print(ret.content)
	data = json.loads(ret.content)
	return JsonResponse(data, safe=False)


